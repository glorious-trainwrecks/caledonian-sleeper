extends Node2D

var basic_tile_scene = load('res://tileset/basic_tile/BasicTile.tscn')

func _spawn_basic_tile(x, y):
	var basic_tile = basic_tile_scene.instance()
	add_child(basic_tile)
	basic_tile.spawn(x, y)
	return basic_tile
	

func _ready():
	_spawn_basic_tile(500, 500)