extends Node2D

var basic_tile_scene = load('res://tileset/basic_tile/BasicTile.tscn')
var BLOCK_HEIGHT = 120
var BLOCK_WIDTH = 120
var MAX_FALL_DEPTH = 3

var _LEVEL_LENGTH
var _LEVEL_HEIGHT
var _MAX_VERTICAL_JUMP
var _MAX_HORIZONTAL_JUMP
var _PLATFORM_LENGTH_RANGE
	
func _spawn_basic_tile(x, y):
	var basic_tile = basic_tile_scene.instance()
	add_child(basic_tile)
	basic_tile.spawn(x, y)
	return basic_tile

func _get_platform_length():
	"""Get a random platform length within the range specified at the beginning."""
	var options = range(_PLATFORM_LENGTH_RANGE[0], _PLATFORM_LENGTH_RANGE[+1])
	return options[randi() % options.size()]
	
func _generate_path():
	"""Generate a series of floating platforms the player will be able to jump between."""
	var block_x = 0
	var block_y = 600
	#var block_y = _LEVEL_HEIGHT * BLOCK_HEIGHT
	var blocks_before_next_jump = _get_platform_length()

	while block_x < _LEVEL_LENGTH * BLOCK_WIDTH:
		_spawn_basic_tile(block_x, block_y)
		
		if blocks_before_next_jump > 0:
			block_x += BLOCK_WIDTH
			blocks_before_next_jump -= 1
		else:
			blocks_before_next_jump = _get_platform_length()
			block_x += BLOCK_WIDTH * (1 + range(_MAX_HORIZONTAL_JUMP)[randi() % _MAX_HORIZONTAL_JUMP])
			
			var reachable_heights = []
			var y = block_y - BLOCK_HEIGHT * _MAX_HORIZONTAL_JUMP
			while (y - block_y < _MAX_VERTICAL_JUMP):
				reachable_heights.append(y)
				y += BLOCK_HEIGHT
			
			block_y = reachable_heights[randi() % reachable_heights.size()]

func _initialise(length, height, max_vertical_jump, max_horizontal_jump, platform_length_range):
	_LEVEL_LENGTH = length
	_LEVEL_HEIGHT = height
	_MAX_VERTICAL_JUMP = max_vertical_jump
	_MAX_HORIZONTAL_JUMP = max_horizontal_jump
	_PLATFORM_LENGTH_RANGE = platform_length_range

func _ready():
	"""Called when the node enters the scene tree for the first time."""
	_initialise(90, 10, 2, 3, Vector2(2, 5))
	_generate_path()