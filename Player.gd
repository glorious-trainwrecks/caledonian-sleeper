extends KinematicBody2D

const GRAVITY = 700
const MOVEMENT_SPEED = 50
const JUMP_ACCELERATION = 500

var velocity = Vector2()

func _ready():
	pass

func _physics_process(delta):
	"""Control character movement"""
	move_and_slide(velocity, Vector2(0, -1))

	if is_on_floor():
		velocity.y = 0
		if Input.is_action_pressed("ui_up"):
			velocity.y -= JUMP_ACCELERATION
	else:
		velocity.y += delta * GRAVITY
	
	if Input.is_action_pressed('ui_right'):
		velocity.x += MOVEMENT_SPEED
	if Input.is_action_pressed('ui_left'):
		velocity.x -= MOVEMENT_SPEED

	
	#move_and_collide(velocity * delta)